# File-Transfer

### about project

> This is that kind of project, where practically everything is 
> defined in project's name. Surprisingly, we will be talking about 
> transmissions of files between some hosts. There are server and client.
> Client can push and pull files and directories from server and have some extended features
> like searching, deleting.

####Requirements:
- Linux or MacOs
- CMake 3.16
- Boost 1.71 
- Protobuf 3.15.2.0
- gRPC 1.37.1

#### get project from gitlab
> git clone https://gitlab.com/LaVuna47/file-transfer.git

#### install dependencies
> generally, CMake and Boost can be found inside your system registry, so following commands must do the job for debian

<i>debian</i>
>>> sudo apt install cmake
>>>
>>> sudo apt install -y libboost-all-dev
>>>
>> another important stuff
>>> sudo apt install -y build-essential autoconf libtool pkg-config
>>

<i> Fedora </i>

// TODO: add description for installing deps for Fedora

<i> MacOS </i>

// TODO: add description for installing deps for Mac

> build, install gRPC
>> to build and install gRPC specify directory where it should be build
>>> export MY_INSTALL_DIR=$HOME/.local
>>
>> Add this directory to your $PATH env variable
>>> PATH=$MY_INSTALL_DIR:$PATH
>>
>> Run script that will do all the job
>>> cd file-transfer
>>
>>> chmod +x ./scripts/install_grpc.sh
>>
>>> sudo MY_INSTALL_DIR=$MY_INSTALL_DIR ./scripts/install_grpc.sh

#### build project
>> cd file-transfer
>
>> mkdir build
> 
>> cd build
> 
>> cmake ..
> 
>> make


#### run unit-tests
> In order to use project you need to export workspace root that is the project root path
>> export WORKSPACE_ROOT=/your/project/root/file-transfer
>
>> cd build
>
>> make test.client && make test.server
> 
>> ./components/test/test/test.client
> 
>> ./components/test/test/test.server

#### file-transfer usage
