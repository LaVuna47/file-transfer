//
// Created by rainier on 04.06.21.
//
#include "Server.h"

namespace po = boost::program_options;
using std::vector, std::string, std::cout;
typedef unsigned short ushort;

namespace co
{ // command options
    string   ip_address;
    uint16_t port;
    std::filesystem::path root_dir_path;
}
/*
==========================
DESCRIPTION:
    Parses arguments from command line and stores them into
    co (command options) namespace. Throws logic_error if at
    least one arg from parameters was missed.
PARAMETERS:
    argc: number of arguments in <argv>
    argv: pointer for line of options.
RETURN:
    void:
==========================
*/
void ParseArguments(int argc, char ** argv)
{
    // Declare the supported options.
    po::options_description desc("Allowed options");
    desc.add_options()
            ("ip", po::value<string>(), "set ip")
            ("port,p",po::value<uint16_t>(), "set port")
            ("root-path",po::value<string>(), "Path were to store files.");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    // ip
    if(vm.count("ip"))
    {
        co::ip_address = vm["ip"].as<string>();
    }
    else
    {
        throw std::logic_error("IP was not specified. Terminated.");
    }
    // port
    if (vm.count("port"))
    {
        co::port = vm["port"].as<uint16_t>();
    }
    else
    {
        throw std::logic_error("Port was not specified. Terminated.");
    }
    // root directory path
    if(vm.count("root-path")){
        co::root_dir_path = vm["root-path"].as<string>();
    }
    else
    {
        throw std::logic_error("Root directory path was not specified. Terminated.");
    }
}


int main(int argc, char ** argv) {
    try
    {
        ParseArguments(argc, argv);
        server::Server myServer(co::ip_address, co::port,co::root_dir_path);
        myServer.Run();
    }
    catch(std::exception& exception)
    {
        std::cerr << exception.what() << '\n';
    }
    catch(...)
    {
        std::cerr << "Unknown exception was thrown.";
    }
}

