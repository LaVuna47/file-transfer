//
// Created by ivan on 10.06.21.
//
// boost
#include "Fixtures.h"

namespace test
{
    ServerFixture::ServerFixture(std::string  host,
                                 uint16_t port,
                                 std::filesystem::path rootPath,
                                 std::filesystem::path serverExe) :
             c_host(std::move(host)),
             c_port(port),
             c_rootPath(std::move(rootPath)),
             c_serverExe(std::move(serverExe))
    {
        Setup();
    }

    ServerFixture::~ServerFixture()
    {
        Teardown();
    }

    void ServerFixture::Setup()
    {
        this->SetupServer();
        this->SetupDirectory();
    }

    void ServerFixture::Teardown()
    {
        // terminate server process
        c_serverProcess.terminate();

        // remove created directory
        std::filesystem::remove_all(c_rootPath);
    }

    void ServerFixture::ShutDownServer()
    {
        Teardown();
    }


    void ServerFixture::SetupServer()
    {
        try
        {
            const std::string& ip_arg = "--ip=" + c_host;
            const std::string& port_arg = "--port=" + std::to_string(c_port);
            const std::string& rootPath_arg = "--root-path=" + c_rootPath.string();
            const std::string& serverExe_arg =  c_serverExe.string();

            // starting process
            c_serverProcess = boost::process::child(serverExe_arg, ip_arg, port_arg, rootPath_arg);
            if(!c_serverProcess.running())
            {
                BOOST_ERROR("Server Process is not running.");
                return;
            }
            // give 0.5 s to warm up
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        }
        catch (std::exception& e)
        {
            BOOST_TEST_MESSAGE(e.what());
            throw std::system_error(ECHILD, std::generic_category(),"Server process creation failed");
        }
        catch(...)
        {
            BOOST_TEST_MESSAGE("Unknown error");
            throw std::system_error(ECHILD, std::generic_category(),"Unknown error");
        }
    }
    void ServerFixture::SetupDirectory()
    {
        std::filesystem::create_directories(c_rootPath);
    }

    ClientFixture::ClientFixture(const std::string& serverHost,
                                 uint16_t serverPort,
                                 const std::string& clientRootPath):
             c_serverHost(serverHost),
             c_serverPort(serverPort),
             c_clientRootPath(clientRootPath)
    {
        Setup();
    }

    ClientFixture::~ClientFixture()
    {
        Teardown();
    }

    void ClientFixture::SetupClient()
    {
        try
        {
            // creating new stub with insecure channel
            c_cliStub = ft::FileTransfer::NewStub(grpc::CreateChannel(c_serverHost + ":" + std::to_string(c_serverPort),
                                                                      grpc::InsecureChannelCredentials()));
        }
        catch(std::exception& e)
        {
            BOOST_ERROR("Exception: " << e.what());
            return;
        }
        catch(...)
        {
            BOOST_ERROR("Exception: Unknown error");
            return;
        }
    }

    void ClientFixture::SetupDirectory()
    {
        std::filesystem::create_directories(c_clientRootPath);
    }

    void ClientFixture::Setup()
    {
        this->SetupClient();
        this->SetupDirectory();
    }

    void ClientFixture::Teardown()
    {
        // removing client stub object
        c_cliStub.reset();

        // removing directories
        std::filesystem::remove_all(c_clientRootPath);
    }

    void ClientFixture::ShutdownClient()
    {
        Teardown();
    }

    std::shared_ptr<ft::FileTransfer::Stub> ClientFixture::GetClientStub()
    {
        return c_cliStub;
    }

    std::shared_ptr<client::IClientManager> ClientFixture::GetClientManager()
    {
        std::shared_ptr<client::IClientManager> clientManager =
                std::make_shared<client::ClientManagerCoreImpl>(c_serverHost, c_serverPort);
        return clientManager;
    }


    void FilesManager::GenerateFile(uint64_t fileSize, const std::filesystem::path& filePath)
    {
        const uint64_t step = 655;
        std::filesystem::path fullFilePath = filePath.parent_path();

        // ensure directory exist
        std::filesystem::create_directories(fullFilePath);

        // opening file
        std::fstream wStream(fullFilePath.string() + "/" + filePath.filename().string(), std::ios::out | std::ios::trunc | std::ios::binary);
        if(!wStream.is_open())
        {
            BOOST_ERROR("Failed to open " << fullFilePath.string() + filePath.filename().string());
            return;
        }

        std::random_device dev;
        std::mt19937 rng(dev());
        std::uniform_int_distribution<std::mt19937::result_type> dist255(1,255); // distribution in range [1, 6]
        char buffer[step];

        for(uint32_t i = 0; i + step <= fileSize; i += step)
        {
            char randChar = static_cast<char>(dist255(rng));
            memset(buffer,randChar,step);
            wStream.write(buffer,step);
        }
        {
            char randChar = static_cast<char>(dist255(rng));
            memset(buffer, randChar, fileSize % step);
            wStream.write(buffer,static_cast<std::streamsize>(fileSize % step));
        }

        wStream.close();
    }


    void FilesManager::GenerateDirectories(const std::filesystem::path &path)
    {
        std::filesystem::create_directories(path.string());
    }

    bool FilesManager::CompareFiles(const std::filesystem::path &p1, const std::filesystem::path &p2)
    {
        // opening file
        std::fstream rStream1( p1.string(), std::ios::in | std::ios::binary);
        if(!rStream1.is_open())
        {
            BOOST_ERROR("Could not open " << p1);
            return false;
        }
        std::fstream rStream2( p2.string(), std::ios::in | std::ios::binary);
        if(!rStream2.is_open())
        {
            BOOST_ERROR("Could not open " << p2);
            return false;
        }
        const uint64_t bufferSize = 65536;
        char buffer1[bufferSize];
        char buffer2[bufferSize];
        memset(buffer1,'\0' ,bufferSize);
        memset(buffer2,'\0' ,bufferSize);

        for(std::streamsize s1, s2;true;)
        {
            s1 = rStream1.readsome(buffer1, bufferSize);
            s2 = rStream2.readsome(buffer2, bufferSize);
            if(memcmp(buffer1, buffer2, s1) != 0 || s1 != s2)
            {
                return false;
            }
            if(s1 < bufferSize || s2 < bufferSize)
            {
                break;
            }
        }
        // closing files
        rStream1.close();
        rStream2.close();
        return true;
    }

    bool FilesManager::CompareDirectories(const std::filesystem::path &p1, const std::filesystem::path &p2)
    {
        std::vector<std::string> paths1,paths2;

        std::filesystem::recursive_directory_iterator iter1(p1), iter2(p2);
        for(;iter1 != std::filesystem::end(iter1); ++iter1)
        {
            const std::string& rel_path = iter1->path().string();
            paths1.emplace_back( rel_path.substr(rel_path.find(p1.string()) + p1.string().size()));
        }
        for(;iter2 != std::filesystem::end(iter2); ++iter2)
        {
            const std::string& rel_path = iter2->path().string();
            paths2.emplace_back(rel_path.substr(rel_path.find(p2.string()) + p2.string().size()));
        }

        std::sort(paths1.begin(), paths1.end());
        std::sort(paths2.begin(), paths2.end());

        if(paths1.size() != paths2.size())
            return false;

        for(int i = 0; i < (int)paths1.size(); ++i)
        {
            if(paths1[i] != paths2[i])
                return false;

            if(std::filesystem::is_regular_file(std::filesystem::path(p1.string() + "/" + paths1[i])))
            {
                std::string f1 = p1.string() +"/" +  paths1[i];
                std::string f2 = p2.string() + "/" + paths2[i];
                if(!FilesManager::CompareFiles(f1, f2))
                {
                    return false;
                }
            }
        }

        return true;
    }


}