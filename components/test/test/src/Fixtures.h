//
// Created by ivan on 10.06.21.
//

#ifndef FILETRANSFER_FIXTURES_H
#define FILETRANSFER_FIXTURES_H

// std
#include <string>
#include <filesystem>
#include <random>
#include <set>

// boost
#include <boost/process.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/beast/http/type_traits.hpp>

// SI.grpc
#include "ft.grpc.pb.h"

// gRPC
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
#include <grpcpp/server_builder.h>

// other
#include "ClientManager.h"

namespace test
{
    /*
    This fixture should create temp directory somewhere, where server will be saving client's files.
    Also this fixture should create server process.
     */
    class ServerFixture
    {
    private:

        void SetupServer();
        void SetupDirectory();
        void Setup();
        void Teardown();

    protected:
        // variables
        std::string c_host;
        uint16_t    c_port;
        std::filesystem::path c_rootPath;
        std::filesystem::path c_serverExe;
        boost::process::child c_serverProcess;

    public:
        // constructor / destructor
        ServerFixture(std::string  host,
                          uint16_t port,
                          std::filesystem::path rootPath,
                          std::filesystem::path serverExe);

        ~ServerFixture();
        // core
        void ShutDownServer();
    };

    /*
     This fixture should create client stub, connect it to server.
     Also should create client directory where will be stored client's files and directories.
     */
    class ClientFixture
    {
    protected:
        // variables
        const std::string& c_serverHost;
        const uint16_t     c_serverPort;
        const std::string& c_clientRootPath;
        /* object to communicate with server */
        std::shared_ptr<ft::FileTransfer::Stub> c_cliStub;

        // methods
        void SetupClient();
        void SetupDirectory();

        void Setup();
        void Teardown();

    public:
        ClientFixture(const std::string& serverHost,
                      uint16_t serverPort,
                      const std::string& clientRootPath);
        ~ClientFixture();

        // core
        void ShutdownClient();
        std::shared_ptr<ft::FileTransfer::Stub> GetClientStub();
        std::shared_ptr<client::IClientManager> GetClientManager();
    };

    class FilesManager
    {
    public:
        /** Default path is user home path **/
        FilesManager()=default;
        ~FilesManager()=default;
        // core

        /* Generates file with specified size relative to root directory.
         * Also generates missing directories if they don't already exist.*/
        static void GenerateFile(uint64_t fileSize, const std::filesystem::path& filePath = "./");
        /* Generates directories */
        static void GenerateDirectories(const std::filesystem::path& path = "./");
        /* Returns true if equals, otherwise false. Compares only content of file.
         * Filenames do not play any role here. p1 and p2 are absolute paths. */
        static bool CompareFiles(const std::filesystem::path& p1, const std::filesystem::path& p2);
        /* Returns true if equals. Checks filesystem structure and corresponding files equivalency.
         * Root directories names may not be equal. p1 and p2 are absolute paths. */
        static bool CompareDirectories(const std::filesystem::path& p1, const std::filesystem::path& p2);

    };
}

#endif //FILETRANSFER_FIXTURES_H
