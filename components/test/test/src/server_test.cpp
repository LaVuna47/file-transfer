#define BOOST_TEST_MODULE Server Test
#include <boost/test/unit_test.hpp>
#include "Fixtures.h"
#include <regex>
#include "Helper.h"
// TODO : write this options into command line options or config file
const std::string& g_serverHost = "127.0.0.1";
const uint16_t     g_serverPort = 5555;
const std::string& g_serverRootPath = std::string(std::getenv("WORKSPACE_ROOT")) + "/.server-root/";
const std::string& g_serverExe = std::string(std::getenv("WORKSPACE_ROOT")) + "/cmake-build-debug/components/server.exe/server.exe";
const std::string& g_clientRootPath = std::string(std::getenv("WORKSPACE_ROOT")) + "/.client-root/";

BOOST_AUTO_TEST_CASE(test_delete_file)
{
    test::ServerFixture serverFixture(g_serverHost, g_serverPort,g_serverRootPath,g_serverExe);
    test::ClientFixture clientFixture(g_serverHost, g_serverPort, g_clientRootPath);

    const std::string& path1 = "file.txt";
    const std::string& path2 = "/some/long/directory/path/file.txt";

    { // creating files
        test::FilesManager::GenerateFile(10203,g_serverRootPath + path1);
        BOOST_TEST(std::filesystem::exists(g_serverRootPath + path1));
        test::FilesManager::GenerateFile(10203,g_serverRootPath + path2);
        BOOST_TEST(std::filesystem::exists(g_serverRootPath + path2));
    }
    auto clientStub = clientFixture.GetClientStub();

    {
        grpc::ClientContext clientContext;
        ft::Response serverResponse;
        ft::FilePath path;
        path.set_path(path1);

        clientStub->DeleteFile(&clientContext, path, &serverResponse);

        BOOST_TEST(serverResponse.status() == ft::Response_STATUS_SUCCESS);
        BOOST_TEST(!boost::filesystem::exists(g_serverRootPath + path1));
    }
    {
        grpc::ClientContext clientContext;
        ft::Response serverResponse;
        ft::FilePath path;
        path.set_path(path2);

        clientStub->DeleteFile(&clientContext, path, &serverResponse);

        BOOST_TEST(serverResponse.status() == ft::Response_STATUS_SUCCESS);
        BOOST_TEST(!boost::filesystem::exists(g_serverRootPath + path1));
    }
}

BOOST_AUTO_TEST_CASE(test_push_file)
{
    test::ServerFixture serverFixture(g_serverHost, g_serverPort, g_serverRootPath, g_serverExe);
    test::ClientFixture clientFixture(g_serverHost, g_serverPort, g_clientRootPath);

    // test appropriate folders existing
    BOOST_TEST(std::filesystem::exists(g_serverRootPath));
    BOOST_TEST(std::filesystem::exists(g_clientRootPath));

    std::vector<std::string> files;

    { // creating clients files
        files.reserve(10);
        for(int i = 1; i <= 8; ++i)
        {
            files.emplace_back(g_clientRootPath + "/file" + std::to_string(i));
            test::FilesManager::GenerateFile(static_cast<uint64_t>(pow(10, i)), files.back());
            // check existence
            BOOST_TEST(std::filesystem::exists(files.back()));
        }
    }

    { // transfer some files
        auto clientStub = clientFixture.GetClientStub();
        for(const auto& filePath: files)
        {
            // init required structures to transfer data
            ft::Response serverResponse;
            grpc::ClientContext clientContext;
            std::string filename = std::filesystem::path(filePath).filename();
            std::unique_ptr<grpc::ClientWriter<ft::FileChunk>> cliWriter(clientStub->PushFile(&clientContext, &serverResponse));
            ft::FileChunk fileChunk;
            fileChunk.set_path(filePath.substr(filePath.find(g_clientRootPath) + g_clientRootPath.size()));

            std::fstream rStream(filePath,std::ios::in | std::ios::binary);
            if(!rStream.is_open())
            {
                BOOST_ERROR("Could not open " << filePath);
                return;
            }
            const uint64_t BUF_SIZE = 65536;
            char buffer[BUF_SIZE];
            memset(buffer,'\0',BUF_SIZE);
            for(std::streamsize bytesRead;true;)
            {
                bytesRead = rStream.readsome(buffer, BUF_SIZE);
                fileChunk.set_datablock(buffer,bytesRead);
                cliWriter->Write(fileChunk);
                fileChunk.clear_datablock();
                if(bytesRead < BUF_SIZE)
                    break;
            }
            cliWriter->WritesDone();
            rStream.close();

            // check status
            grpc::Status status = cliWriter->Finish();
            BOOST_TEST(status.ok());
            BOOST_TEST(serverResponse.status() == ft::Response_STATUS_SUCCESS);

            // check if files really transferred
            BOOST_TEST(std::filesystem::exists(g_serverRootPath + filename));
            BOOST_TEST(test::FilesManager::CompareFiles(filePath, g_serverRootPath + filename));
        }
    }

}

BOOST_AUTO_TEST_CASE(test_pull_file)
{
    test::ServerFixture serverFixture(g_serverHost, g_serverPort,g_serverRootPath,g_serverExe);
    test::ClientFixture clientFixture(g_serverHost,g_serverPort,g_clientRootPath);

    std::vector<std::filesystem::path> files;
    const int filesNum = 8;

    { // creating some files on server's side
        for (int i = 1; i <= filesNum; ++i)
        {
            const std::string file = g_serverRootPath + "/file" + std::to_string(i);
            test::FilesManager::GenerateFile(static_cast<int>(pow(10,i)),file);
            BOOST_CHECK(std::filesystem::exists(file));
            files.emplace_back(file);
        }
    }

    { // pulling created files and checking if they are correct

        auto clientStub = clientFixture.GetClientStub();
        for(const auto& file: files)
        {
            ft::FilePath filePath;
            filePath.set_path(file.filename());
            grpc::ClientContext clientContext;
            std::unique_ptr<grpc::ClientReader<ft::FileChunk>> reader(clientStub->PullFile(&clientContext, filePath));

            // opening wStream
            std::fstream wStream(g_clientRootPath + file.filename().string(), std::ios::out | std::ios::binary | std::ios::trunc);
            if(!wStream.is_open())
            {
                BOOST_ERROR("Could not open " << g_clientRootPath + file.filename().string());
                return;
            }

            ft::FileChunk fileChunk;

            const int bufSize = 65536;
            char buffer[bufSize];
            memset(buffer,'\0',bufSize);

            for(int cntr = 0;reader->Read(&fileChunk); ++cntr)
            {
                std::string str = fileChunk.datablock();
                wStream.write(fileChunk.mutable_datablock()->c_str(), static_cast<std::streamsize>(fileChunk.datablock().size()));
            }

            wStream.close();
            grpc::Status status = reader->Finish();
            BOOST_TEST(status.ok());
            BOOST_TEST(std::filesystem::exists(g_clientRootPath + file.filename().string()));
            BOOST_TEST(test::FilesManager::CompareFiles(file,g_clientRootPath + file.filename().string()));
        }
    }
}


BOOST_AUTO_TEST_CASE(test_extract)
{
    test::ServerFixture serverFixture(g_serverHost, g_serverPort,g_serverRootPath,g_serverExe);
    test::ClientFixture clientFixture(g_serverHost,g_serverPort,g_clientRootPath);

    { // creating directory, file
        test::FilesManager::GenerateFile(101892, g_clientRootPath + "/file");
        test::FilesManager::GenerateDirectories(g_clientRootPath + "/some/strange1/directory");
        test::FilesManager::GenerateDirectories(g_clientRootPath + "/some/strange2/directory");
    }
    { // archiving them
        helper::Archive(g_clientRootPath + "/some",g_serverRootPath + "/some.zip");
        helper::Archive(g_clientRootPath + "/file",g_serverRootPath + "/file.zip");
        BOOST_TEST(std::filesystem::exists(g_serverRootPath + "/some.zip"));
        BOOST_TEST(std::filesystem::exists(g_serverRootPath + "/file.zip"));
    }
    { // extracting
        auto clientStub = clientFixture.GetClientStub();

        {
            grpc::ClientContext clientContext;
            ft::Response serverResponse;
            ft::ArchivePath archivePath;

            archivePath.set_path("some.zip");
            clientStub->Extract(&clientContext, archivePath, &serverResponse);
            BOOST_TEST(serverResponse.status() == ft::Response_STATUS_SUCCESS);
            BOOST_TEST(std::filesystem::exists(g_serverRootPath + "/some"));
            BOOST_TEST(test::FilesManager::CompareDirectories(g_clientRootPath + "/some", g_serverRootPath + "/some"));
        }
        {
            grpc::ClientContext clientContext;
            ft::Response serverResponse;
            ft::ArchivePath archivePath;

            archivePath.set_path("file.zip");
            clientStub->Extract(&clientContext, archivePath, &serverResponse);
            BOOST_TEST(serverResponse.status() == ft::Response_STATUS_SUCCESS);
            BOOST_TEST(std::filesystem::exists(g_serverRootPath + "/file"));
            BOOST_TEST(test::FilesManager::CompareFiles(g_clientRootPath + "/file", g_serverRootPath + "/file"));
        }

    }
}


BOOST_AUTO_TEST_CASE(test_search)
{
    test::ServerFixture serverFixture(g_serverHost, g_serverPort,g_serverRootPath,g_serverExe);
    test::ClientFixture clientFixture(g_serverHost, g_serverPort, g_clientRootPath);

    std::vector<std::string> files;
    const std::string& dirName = "dir";

    { // creating files and directories
        for (int i = 0; i < 5; ++i)
        {
            const std::string& filename = "coolfile" + std::to_string(i) + ".pdf";
            test::FilesManager::GenerateFile(101010, g_serverRootPath + filename);
            files.emplace_back(filename);
        }
        for (int i = 0; i < 5; ++i)
        {
            const std::string& filename = "normalfile" + std::to_string(i) + ".png";
            test::FilesManager::GenerateFile(101010, g_serverRootPath + filename);
            files.emplace_back(filename);
        }
        for (int i = 0; i < 5; ++i)
        {
            const std::string& filename = "coolfile" + std::to_string(i) + ".txt";
            test::FilesManager::GenerateFile(101010, g_serverRootPath + filename);
            files.emplace_back(filename);
        }
        BOOST_TEST(std::filesystem::exists(g_serverRootPath + "normalfile0.png"));
        test::FilesManager::GenerateDirectories(g_serverRootPath + dirName);
        for (int i = 0; i < 5; ++i)
        {
            const std::string& filename = dirName + "/" + "file" + std::to_string(i) + ".pdf";
            test::FilesManager::GenerateFile(101010, g_serverRootPath + filename);
            files.emplace_back(filename);
        }
        for (int i = 0; i < 5; ++i)
        {
            const std::string& filename = dirName + "/" +"normalfile" + std::to_string(i) + ".png";
            test::FilesManager::GenerateFile(101010, g_serverRootPath + filename);
            files.emplace_back(filename);
        }
        for (int i = 0; i < 5; ++i)
        {
            const std::string& filename = dirName + "/" + "coolfile" + std::to_string(i) + ".txt";
            test::FilesManager::GenerateFile(101010, g_serverRootPath + filename);
            files.emplace_back(filename);
        }
        BOOST_TEST(std::filesystem::exists(g_serverRootPath + dirName + "/" +"normalfile0.png"));
    }

    auto getMatchedResult = [&files](const std::string& reg) -> std::vector<std::string>
    {
        std::vector<std::string> trueResult;
        std::regex regex(reg);
        for(const auto& file: files)
        {
            if(std::regex_match(file, regex))
            {
                trueResult.push_back(file);
            }
        }
        std::sort(trueResult.begin(), trueResult.end());
        return trueResult;
    };

    auto test_dataset = [&clientFixture, &getMatchedResult](const std::string& l_regex) -> void
    {
        const std::vector<std::string>& trueRes = getMatchedResult(l_regex);
        std::vector<std::string> queryRes;

        auto clientStub = clientFixture.GetClientStub();
        grpc::ClientContext clientContext;
        ft::Regex regex;
        regex.set_regex(l_regex);
        std::unique_ptr<grpc::ClientReader<ft::FilePath>> reader(clientStub->Search(&clientContext, regex));

        ft::FilePath path;
        for(;reader->Read(&path);)
        {
            queryRes.emplace_back(path.path());
        }
        grpc::Status status = reader->Finish();

        BOOST_TEST(status.ok());
        std::sort(queryRes.begin(), queryRes.end());
        BOOST_TEST(queryRes == trueRes);
    };

    { // searching for files that end on [.txt]
        const std::string& l_regex = ".*\\.txt";
        test_dataset(l_regex);
    }

    { // searching for files that start with [cool].
        const std::string &l_regex = ".*cool.*";
        test_dataset(l_regex);
    }

    { // searching for files that start with dir and end with .png.
        const std::string& l_regex = ".*" + dirName + "/.*\\.png";
        test_dataset(l_regex);
    }
}

BOOST_AUTO_TEST_CASE(test_delete_directory)
{
    test::ServerFixture serverFixture(g_serverHost, g_serverPort,g_serverRootPath,g_serverExe);
    test::ClientFixture clientFixture(g_serverHost, g_serverPort, g_clientRootPath);

    const std::string& path1 = "test-dir";
    const std::string& path2 = "some/dir/to/be/deleted";

    { // create some directories
        test::FilesManager::GenerateDirectories(g_serverRootPath + path1);
        BOOST_TEST(std::filesystem::exists(g_serverRootPath + path1));
        test::FilesManager::GenerateDirectories(g_serverRootPath + path2);
        BOOST_TEST(std::filesystem::exists(g_serverRootPath + path2));
    }

    auto clientStub = clientFixture.GetClientStub();

    { // path1
        grpc::ClientContext clientContext;
        ft::Response serverResponse;
        ft::DirectoryPath path;

        path.set_path(path1);
        clientStub->DeleteDirectory(&clientContext, path, &serverResponse);

        BOOST_TEST(serverResponse.status() == ft::Response_STATUS_SUCCESS);
        BOOST_TEST(!boost::filesystem::exists(g_serverRootPath + path.path()));
    }

    { // path2
        grpc::ClientContext clientContext;
        ft::Response serverResponse;
        ft::DirectoryPath path;
        path.set_path(path2);

        clientStub->DeleteDirectory(&clientContext, path, &serverResponse);

        BOOST_TEST(serverResponse.status() == ft::Response_STATUS_SUCCESS);
        BOOST_TEST(!boost::filesystem::exists(g_serverRootPath + path.path()));
        BOOST_TEST(boost::filesystem::exists(g_serverRootPath +
                    boost::filesystem::path(path.path()).parent_path().string()));

    }
}

// TODO: write more test cases
BOOST_AUTO_TEST_CASE(test_delete)
{
    test::ServerFixture serverFixture(g_serverHost, g_serverPort,g_serverRootPath,g_serverExe);
    test::ClientFixture clientFixture(g_serverHost, g_serverPort, g_clientRootPath);

    std::vector<std::string> allFiles;
    const std::string& dirName = "dir";
    { // creating files and directories
        for (int i = 0; i < 5; ++i)
        {
            const std::string& filename = "coolfile" + std::to_string(i) + ".pdf";
            test::FilesManager::GenerateFile(101010, g_serverRootPath + filename);
            allFiles.emplace_back(filename);
        }
        for (int i = 0; i < 5; ++i)
        {
            const std::string& filename = "normalfile" + std::to_string(i) + ".png";
            test::FilesManager::GenerateFile(101010, g_serverRootPath + filename);
            allFiles.emplace_back(filename);
        }
        for (int i = 0; i < 5; ++i)
        {
            const std::string& filename = "coolfile" + std::to_string(i) + ".txt";
            test::FilesManager::GenerateFile(101010, g_serverRootPath + filename);
            allFiles.emplace_back(filename);
        }
        BOOST_TEST(std::filesystem::exists(g_serverRootPath + "normalfile0.png"));
        test::FilesManager::GenerateDirectories(g_serverRootPath + dirName);
        for (int i = 0; i < 5; ++i)
        {
            const std::string& filename = dirName + "/" + "file" + std::to_string(i) + ".pdf";
            test::FilesManager::GenerateFile(101010, g_serverRootPath + filename);
            allFiles.emplace_back(filename);
        }
        for (int i = 0; i < 5; ++i)
        {
            const std::string& filename = dirName + "/" +"normalfile" + std::to_string(i) + ".png";
            test::FilesManager::GenerateFile(101010, g_serverRootPath + filename);
            allFiles.emplace_back(filename);
        }
        for (int i = 0; i < 5; ++i)
        {
            const std::string& filename = dirName + "/" + "coolfile" + std::to_string(i) + ".txt";
            test::FilesManager::GenerateFile(101010, g_serverRootPath + filename);
            allFiles.emplace_back(filename);
        }
        BOOST_TEST(std::filesystem::exists(g_serverRootPath + dirName + "/" +"normalfile0.png"));
    }

    auto  checkAllFilesByRegex = [&allFiles](const std::string& regex) -> std::vector<std::string>
    {
        std::regex e(regex);
        std::vector<std::string> res;
        for(const auto& file: allFiles)
        {
            if(std::regex_match(file, e))
            {
                res.emplace_back(file);
            }
        }
        return res;
    };

    auto clientStub = clientFixture.GetClientStub();

    { // delete all files that end with .png
        const std::string& reg = ".*\\.png";
        const std::vector<std::string>& toBeDeleted = checkAllFilesByRegex(reg);

        grpc::ClientContext clientContext;
        ft::Response serverResponse;
        ft::Regex regex;
        regex.set_regex(reg);

        clientStub->Delete(&clientContext, regex, &serverResponse);

        BOOST_TEST(serverResponse.status() == ft::Response_STATUS_SUCCESS);

        // check if deleted
        for(const auto& item: toBeDeleted)
        {
            BOOST_TEST(!std::filesystem::exists(g_serverRootPath + item));
        }
    }
}

BOOST_AUTO_TEST_CASE(test_archive)
{
    test::ServerFixture serverFixture(g_serverHost, g_serverPort,g_serverRootPath,g_serverExe);
    test::ClientFixture clientFixture(g_serverHost, g_serverPort, g_clientRootPath);

    // TODO: test Archive() server method
}









