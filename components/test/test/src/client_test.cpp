#define BOOST_TEST_MODULE ClientTest
#include <boost/test/unit_test.hpp>
#include "Fixtures.h"
#include <regex>
#include "ClientManager.h"
#include "Log.h"
const std::string& g_serverHost = "127.0.0.1";
const uint16_t     g_serverPort = 5555;
const std::string& g_serverRootPath = std::string(std::getenv("WORKSPACE_ROOT")) + "/.server-root/";
const std::string& g_serverExe = std::string(std::getenv("WORKSPACE_ROOT")) + "/cmake-build-debug/components/server.exe/server.exe";
const std::string& g_clientRootPath = std::string(std::getenv("WORKSPACE_ROOT")) + "/.client-root/";

void Clean(const std::filesystem::path& path)
{
    // insure to not delete important stuff
    if(path.filename().string() == std::filesystem::path(g_serverRootPath).filename().string())
        helper::Execute("rm -rf " + path.string() + "/*");
}

BOOST_AUTO_TEST_CASE(test_PushFile)
{
    test::ServerFixture serverFixture(g_serverHost, g_serverPort,g_serverRootPath,g_serverExe);
    test::ClientFixture clientFixture(g_serverHost, g_serverPort, g_clientRootPath);
    auto clientManager = clientFixture.GetClientManager();

    std::vector<std::filesystem::path> paths = {"file1.txt", "deep/directory/file2.pdf"};
    for(const auto& path: paths)
    {
        test::FilesManager::GenerateFile(1020,g_clientRootPath + path.string());
        ft::Response response = clientManager->PushFile(g_clientRootPath + path.string(),path.parent_path());
        std::cout << "Response: " << response.message() << '\n' << std::flush;
        // testing
        BOOST_TEST(response.status() == ft::Response::SUCCESS);
        BOOST_TEST(std::filesystem::exists(g_serverRootPath + path.string()));
        BOOST_TEST(test::FilesManager::CompareFiles(g_serverRootPath + path.string(),
                                         g_clientRootPath + path.string()));
    }
}

BOOST_AUTO_TEST_CASE(test_PullFile)
{
    test::ServerFixture serverFixture(g_serverHost, g_serverPort,g_serverRootPath,g_serverExe);
    test::ClientFixture clientFixture(g_serverHost, g_serverPort, g_clientRootPath);
    auto clientManager = clientFixture.GetClientManager();

    std::vector<std::filesystem::path> paths {"file.pdf", "A/B/file.pdf", "/F/L/file.pdf" ,"./F/Ka/file.pdf" };
    for(const auto& path: paths)
    {
        test::FilesManager::GenerateFile(10101,g_serverRootPath + path.string());
        ft::Response response = clientManager->PullFile(g_clientRootPath + path.parent_path().string(), path);
        LOG_STATUS(response.message());
        // testing
        BOOST_TEST(response.status() == ft::Response::SUCCESS);
        BOOST_TEST(std::filesystem::exists(g_clientRootPath + path.string()));
        BOOST_TEST(test::FilesManager::CompareFiles(g_serverRootPath + path.string(),
                                                    g_clientRootPath + path.string()));
    }

    Clean(g_serverRootPath);
}

BOOST_AUTO_TEST_CASE(test_PushFiles)
{
    test::ServerFixture serverFixture(g_serverHost, g_serverPort,g_serverRootPath,g_serverExe);
    test::ClientFixture clientFixture(g_serverHost, g_serverPort, g_clientRootPath);
    auto clientManager = clientFixture.GetClientManager();

    std::vector<std::filesystem::path> paths = {"file1.txt", "deep/directory/file2.pdf"};
    std::vector<std::tuple<std::filesystem::path, std::filesystem::path>> client_serverPaths;

    for(const auto& path: paths)
    {
        test::FilesManager::GenerateFile(2890,g_clientRootPath + path.string());
        client_serverPaths.emplace_back(g_clientRootPath + path.string(), path.parent_path());
    }

    ft::Response response = clientManager->PushFiles(client_serverPaths);
    BOOST_TEST(response.status() == ft::Response::SUCCESS);

    for(const auto& path: paths)
    {
        // testing
        BOOST_TEST(std::filesystem::exists(g_serverRootPath + path.string()));
        BOOST_TEST(test::FilesManager::CompareFiles(g_serverRootPath + path.string(),
                                                    g_clientRootPath + path.string()));
    }
}

BOOST_AUTO_TEST_CASE(test_PullFiles)
{
    test::ServerFixture serverFixture(g_serverHost, g_serverPort,g_serverRootPath,g_serverExe);
    test::ClientFixture clientFixture(g_serverHost, g_serverPort, g_clientRootPath);
    auto clientManager = clientFixture.GetClientManager();

    std::vector<std::filesystem::path> paths {"file.pdf", "A/B/file.pdf", "/F/L/file.pdf" ,"./F/Ka/file.pdf" };
    std::vector<std::tuple<std::filesystem::path, std::filesystem::path>> client_serverPaths;
    for(const auto& path: paths)
    {
        test::FilesManager::GenerateFile(3678,g_serverRootPath + path.string());
        client_serverPaths.emplace_back(g_clientRootPath + path.parent_path().string(), path);
    }

    ft::Response response = clientManager->PullFiles(client_serverPaths);
    BOOST_TEST(response.status() == ft::Response::SUCCESS);

    for(const auto& path: paths)
    {
        BOOST_TEST(std::filesystem::exists(g_clientRootPath + path.string()));
        BOOST_TEST(test::FilesManager::CompareFiles(g_serverRootPath + path.string(),
                                                    g_clientRootPath + path.string()));
    }
}

BOOST_AUTO_TEST_CASE(test_PushDirectory)
{
    auto getRootDirectory = [&](const std::string& str) -> std::string
    {
        if(str.empty())
            return {"."};

        std::string res;
        int i = 0;
        while(str[i] == '/' && !str.empty()) ++i;
        for(; i < (int)str.size(); ++i)
        {
            if(str[i] == '/') break;
            res.push_back(str[i]);
        }
        return res;
    };
    //    test::ServerFixture serverFixture(g_serverHost, g_serverPort,g_serverRootPath,g_serverExe);
    test::ClientFixture clientFixture(g_serverHost, g_serverPort, g_clientRootPath);
    auto clientManager = clientFixture.GetClientManager();
    // TODO: figure out mistakes
    std::vector<std::filesystem::path> dirs {"someDir"};

    for(const auto& dir: dirs)
    {
        test::FilesManager::GenerateDirectories(g_clientRootPath + dir.string());
        std::string root_dir = getRootDirectory(dir.string());

        ft::Response response = clientManager->PushDirectory(g_clientRootPath + root_dir,".");
        LOG_STATUS(response.message());
        BOOST_TEST(response.status() == ft::Response::SUCCESS);
        BOOST_TEST(std::filesystem::exists(g_serverRootPath + dir.root_name().string()));
    }


    Clean(g_serverRootPath);
}

BOOST_AUTO_TEST_CASE(test_PullDirectory)
{
    // TODO:
}

BOOST_AUTO_TEST_CASE(test_PushDirectories)
{
    // TODO:
}

BOOST_AUTO_TEST_CASE(test_PullDirectories)
{
    // TODO:
}

BOOST_AUTO_TEST_CASE(test_SearchForFiles)
{
    // TODO:
}

BOOST_AUTO_TEST_CASE(test_DeleteFile)
{
    // TODO:
}

BOOST_AUTO_TEST_CASE(test_DeleteFiles)
{
    // TODO:
}

BOOST_AUTO_TEST_CASE(test_DeleteDirectory)
{
    // TODO:
}

BOOST_AUTO_TEST_CASE(test_DeleteDirectories)
{
    // TODO:
}

BOOST_AUTO_TEST_CASE(test_Delete)
{
    // TODO:
}

BOOST_AUTO_TEST_CASE(test_GetFilesystemTreeView)
{
    // TODO:
}