//
// Created by ivan on 16.07.21.
//

#define BOOST_TEST_MODULE HelperTest
#include <boost/test/unit_test.hpp>
#include "Helper.h"

BOOST_AUTO_TEST_CASE(test_Execute)
{
    const std::string directory( std::string(std::getenv("HOME")) + "/temporary_dir");

    helper::Execute("mkdir " + directory);
    BOOST_TEST(std::filesystem::exists(directory));
    helper::Execute("rmdir " + directory);
    BOOST_TEST(!std::filesystem::exists(directory));
}

BOOST_AUTO_TEST_CASE(test_Archive)
{
    /// TODO: test this better
    const std::string directory( std::string(std::getenv("HOME")) + "/temporary_dir");
    helper::Execute("mkdir " + directory);

    const std::string& source = directory;
    const std::string& dest = std::string(std::getenv("HOME")) + "/Desktop/file.zip";

    helper::Archive(source,dest);
    BOOST_TEST(std::filesystem::exists(dest));
    std::filesystem::remove(dest);
}

BOOST_AUTO_TEST_CASE(test_Extract)
{
    /// TODO: test

}









