//
// Created by ivan on 16.06.21.
//

#ifndef FILETRANSFER_SERVER_H
#define FILETRANSFER_SERVER_H

// std
#include <string>
#include <filesystem>
#include <iostream>
// boost
#include <boost/program_options.hpp>

#include "FTServiceImpl.h"

/*
 SOME_DESCRIPTION
 */

namespace server
{
    class Server
    {
    private:
        // variables
        std::string m_hostAddress;
        std::filesystem::path m_rootDirectoryPath;
        uint16_t m_port;

    public:
        Server(std::string  hostAddress,
               uint16_t port,
               std::filesystem::path  rootDirPath);
        Server(const std::filesystem::path &confFile);

        ~Server() = default;

        void Run();
    };

// TODO: implement default server. Implement in config server
    class DefaultServer
    {
    private:

    public:
        DefaultServer() = default;

        ~DefaultServer() = default;
    };
}
#endif //FILETRANSFER_SERVER_H
