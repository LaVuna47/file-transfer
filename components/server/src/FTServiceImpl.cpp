//
// Created by ivan on 02.06.21.
//
#include "FTServiceImpl.h"
#include <boost/process.hpp>
#include <boost/regex.hpp>
#include <regex>
#include "Helper.h"
#include "Log.h"
namespace server
{
    const std::uint32_t BUF_SIZE = 65536;
    FTServiceImpl::FTServiceImpl(const std::filesystem::path &rootDirPath):
            m_rootDirectoryPath(rootDirPath)
    {
        /// TODO: migrate server setup logic to some other module or component
        std::filesystem::create_directories(m_rootDirectoryPath);
    }

    grpc::Status FTServiceImpl::PushFile(grpc::ServerContext *context,
                                         grpc::ServerReader<ft::FileChunk> *reader,
                                         ft::Response *response)
    {
        try
        {
            // chunk to read from client
            ft::FileChunk chunk;
            int bytesDelivered = 0;
            std::string filePath;

            // server sets filename according to first file chunk name
            if (!reader->Read(&chunk))
            {
                LOG_ERROR("Send 0 file chunks");
                return {grpc::StatusCode::INVALID_ARGUMENT, "No files where specified.", "You need to specify files."};
            }
            else
            {
                filePath = chunk.path();
            }
            if (filePath.empty())
            {
                LOG_ERROR("Name of file could not be empty!");
                return {grpc::StatusCode::INVALID_ARGUMENT, "Name of file cannot be empty!", "You need to specify filename"};
            }

            if(!std::filesystem::exists(m_rootDirectoryPath.string() + "/" + std::filesystem::path(filePath).parent_path().string()))
                std::filesystem::create_directories(m_rootDirectoryPath.string() + "/" + std::filesystem::path(filePath).parent_path().string());

            // opening stream to write
            std::fstream wStream(m_rootDirectoryPath.string() + "/" + filePath, std::ios::out | std::ios::binary | std::ios::trunc);
            if (!wStream.is_open())
            {
                LOG_ERROR("Could not open " + m_rootDirectoryPath.string() + "/" + filePath);
                return  {grpc::StatusCode::INTERNAL, "Could not open " + m_rootDirectoryPath.string() + "/" + filePath};
            }

            // writing first chunk to file
            wStream.write(chunk.mutable_datablock()->c_str(), static_cast<int>(chunk.mutable_datablock()->size()));
            bytesDelivered += static_cast<int>(chunk.datablock().size());

            // reading
            for (;reader->Read(&chunk);)
            {
                wStream.write(chunk.mutable_datablock()->c_str(), static_cast<int>(chunk.mutable_datablock()->size()));
                bytesDelivered += static_cast<int>(chunk.datablock().size());
            }
            // closing stream
            wStream.close();

            response->set_message("File " + filePath + " was delivered successfully. File size: " \
                + grpc::to_string(bytesDelivered) + '\n');

            LOG_MESSAGE("File " + m_rootDirectoryPath.string() + "/" + filePath + " was successfully pushed");
            return grpc::Status::OK;
        }
        catch(std::exception& e)
        {
            LOG_EXCEPTION(e.what());
            return {grpc::StatusCode::INTERNAL, e.what()};
        }
        catch(...)
        {
            LOG_EXCEPTION("Unknown error.");
            return {grpc::StatusCode::UNKNOWN, "Unknown exception occurred."};
        }
    }

    grpc::Status FTServiceImpl::PullFile(grpc::ServerContext *context,
                                         const ft::FilePath *filePath,
                                         grpc::ServerWriter<ft::FileChunk> *writer)
    {
        try
        {
            // checking if file does exist
            if(!std::filesystem::exists(m_rootDirectoryPath.string() + "/" + filePath->path()))
            {
                const std::string& msg = "File " + filePath->path() + " does not exist.";
                LOG_ERROR(msg);
                return {grpc::StatusCode::INVALID_ARGUMENT, "File " + filePath->path() + " does not exist.", "You need to write correct filepath."};
            }
            // opening file to read from
            std::fstream rStream(m_rootDirectoryPath.string() + "/" + filePath->path(), std::ios::binary | std::ios::in);
            if (!rStream.is_open())
            {
                const std::string& msg = "File " + filePath->path() + " was not opened.\n";
                LOG_ERROR(msg)
                return {grpc::StatusCode::INTERNAL, "File " + filePath->path() + " was not opened.\n"};
            }
            ft::FileChunk chunk;
            // creating buffer to read data into
            char buf[BUF_SIZE];
            memset(buf, '\0', BUF_SIZE);
            // transferring into following order: rStream -> buf -> chunk -> host
            for (uint32_t pos = 0; true; ++pos)
            {
                auto bytesRead = static_cast<uint32_t>(rStream.readsome(buf, BUF_SIZE));
                chunk.mutable_datablock()->append(buf, bytesRead);
                writer->Write(chunk);
                chunk.mutable_datablock()->clear();
                if (bytesRead < BUF_SIZE)
                {
                    break;
                }
            }
            // closing stream
            rStream.close();
            LOG_MESSAGE("File " + m_rootDirectoryPath.string() + "/" + filePath->path() + " was successfully pulled");
            return grpc::Status::OK;
        }
        catch(std::exception& e)
        {
            LOG_EXCEPTION(e.what());
            return {grpc::StatusCode::INTERNAL, e.what()};
        }
        catch(...)
        {
            LOG_EXCEPTION("Unknown error.");
            return {grpc::StatusCode::UNKNOWN, "Unknown exception occurred."};
        }
    }

    grpc::Status FTServiceImpl::Extract(
            grpc::ServerContext *context,
            const ft::ArchivePath *archivePath,
            ft::Response *response)
    {

        if(!std::filesystem::exists(m_rootDirectoryPath.string() + "/" + archivePath->path()))
        {
            LOG_ERROR("Archive with specified name " + archivePath->path() + " does not exist.")
            return { grpc::StatusCode::INVALID_ARGUMENT, "Archive with specified name " + archivePath->path() + " does not exist."};
        }
        try
        {
            const std::string destinationDir = m_rootDirectoryPath.string() + "/" +
                    std::filesystem::path(archivePath->path()).parent_path().string();
            const std::string sourceFile = m_rootDirectoryPath.string() + "/" + archivePath->path();

            boost::process::system(
                    "unzip " +  sourceFile + " -d " + destinationDir);

            response->set_status(ft::Response_STATUS_SUCCESS);
            response->set_message("Archive " + archivePath->path() + "successfully extracted");
            LOG_MESSAGE("Archive " + archivePath->path() + "successfully extracted")
            return grpc::Status::OK;
        }
        catch(std::exception& e)
        {
            LOG_EXCEPTION(e.what());
            return {grpc::StatusCode::INTERNAL, e.what()};
        }
        catch(...)
        {
            LOG_EXCEPTION("Unknown error.");
            return {grpc::StatusCode::UNKNOWN, "Unknown exception occurred."};
        }
    }


    grpc::Status FTServiceImpl::Archive(
            grpc::ServerContext *context,
            const ft::DirectoryPath *dirPath,
            ft::Response *response)
    {
        if(!std::filesystem::exists(m_rootDirectoryPath.string() + "/" + dirPath->path()))
        {
            LOG_ERROR("Archive with specified name " + dirPath->path() + " does not exist.")
            return {grpc::StatusCode::INVALID_ARGUMENT,"Archive with specified name " + dirPath->path() + " does not exist.", "You need to specify archive name." };
        }
        try
        {
            std::string dirName = std::filesystem::path(dirPath->path()).filename().string();
            helper::Archive(m_rootDirectoryPath.string() + "/" + dirPath->path(),m_rootDirectoryPath.string() + "/" + dirName + ".ft_zip");

            response->set_status(ft::Response_STATUS_SUCCESS);
            response->set_message("Directory " + dirPath->path() + "successfully archived");

            LOG_MESSAGE("Directory " + dirPath->path() + "successfully archived");

            return grpc::Status::OK;
        }
        catch(std::exception& e)
        {
            LOG_EXCEPTION(e.what());
            return {grpc::StatusCode::INTERNAL, e.what()};
        }
        catch(...)
        {
            LOG_EXCEPTION("Unknown error.");
            return {grpc::StatusCode::UNKNOWN, "Unknown exception occurred."};
        }
    }

    grpc::Status FTServiceImpl::Search(grpc::ServerContext *context,
                                       const ft::Regex *regex,
                                       grpc::ServerWriter<ft::FilePath> *writer)
    {
        try
        {

            ft::FilePath filePath;
            std::filesystem::recursive_directory_iterator iter(m_rootDirectoryPath);

            const std::regex e(m_rootDirectoryPath.string() + regex->regex());

            for(;iter != std::filesystem::end(iter); ++iter)
            {
                if(std::regex_match(iter->path().string(),e))
                {
                    const std::string& p = iter->path().string();
                    uint64_t pos = p.find(m_rootDirectoryPath.string());
                    filePath.set_path(p.substr(pos + m_rootDirectoryPath.string().size()));
                    writer->Write(filePath);
                }
            }
            LOG_STATUS("Searching occurred")
            return grpc::Status::OK;
        }
        catch(std::exception& e)
        {
            LOG_EXCEPTION(e.what());
            return {grpc::StatusCode::INTERNAL, e.what()};
        }
        catch(...)
        {
            LOG_EXCEPTION("Unknown error.");
            return {grpc::StatusCode::UNKNOWN, "Unknown exception occurred."};
        }
    }

    grpc::Status FTServiceImpl::DeleteFile(grpc::ServerContext *context,
                                           const ft::FilePath *filePath,
                                           ft::Response *response)
    {
        try
        {
            if (std::filesystem::exists(
                    std::filesystem::absolute(m_rootDirectoryPath).string() + "/" + filePath->path()))
            {
                std::filesystem::remove(std::filesystem::absolute(m_rootDirectoryPath).string() + "/" + filePath->path());
                response->set_message("File " + filePath->path() + " was deleted successfully");
                response->set_status(ft::Response_STATUS_SUCCESS);
                LOG_MESSAGE( "File " + filePath->path() + " was deleted successfully");
                return grpc::Status::OK;
            }
            else
            {
                LOG_ERROR( "File " + filePath->path() + " was not found");
                return {grpc::StatusCode::INVALID_ARGUMENT, "File " + filePath->path() + " was not found", "You need to specify correct file path."};
            }
        }
        catch(std::exception& e)
        {
            LOG_EXCEPTION(e.what());
            return {grpc::StatusCode::INTERNAL, e.what()};
        }
        catch(...)
        {
            LOG_EXCEPTION("Unknown error.");
            return {grpc::StatusCode::UNKNOWN, "Unknown exception occurred."};
        }
    }

    grpc::Status FTServiceImpl::DeleteDirectory(grpc::ServerContext *context,
                                                const ft::DirectoryPath *dirPath,
                                                ft::Response *response)
    {
        try
        {
            if(std::filesystem::exists(m_rootDirectoryPath.string() + "/" + dirPath->path()))
            {
                std::filesystem::remove_all(m_rootDirectoryPath.string() + "/" + dirPath->path());
                response->set_message("Directory " + dirPath->path() + " was deleted successfully");
                response->set_status(ft::Response_STATUS_SUCCESS);
                LOG_MESSAGE("Directory " + dirPath->path() + " was deleted successfully");
                return grpc::Status::OK;
            }
            else
            {
                LOG_ERROR("Directory " + dirPath->path() + " was not found")
                return {grpc::StatusCode::INVALID_ARGUMENT,"Directory " + dirPath->path() + " was not found", "You need to specify correct directory path." };
            }
        }
        catch(std::exception& e)
        {
            LOG_EXCEPTION(e.what());
            return {grpc::StatusCode::INTERNAL, e.what()};
        }
        catch(...)
        {
            LOG_EXCEPTION("Unknown error.");
            return {grpc::StatusCode::UNKNOWN, "Unknown exception occurred."};
        }

    }

    grpc::Status FTServiceImpl::Delete(grpc::ServerContext *context,
                                       const ft::Regex *regex,
                                       ft::Response *response)
    {
        try
        {
            std::filesystem::recursive_directory_iterator iter(m_rootDirectoryPath);
            const boost::regex e(m_rootDirectoryPath.string() + regex->regex());
            int cntr = 0;
            for(;iter != std::filesystem::end(iter); ++iter)
            {
                if(iter->is_regular_file())
                {
                    if(boost::regex_match(iter->path().string(), e))
                    {
                        std::filesystem::remove(*iter);
                        ++cntr;
                    }
                }
            }
            response->set_status(ft::Response_STATUS_SUCCESS);
            response->set_message(std::to_string(cntr) + " files were deleted.");
            LOG_STATUS("Delete by regex occurred.");
            return grpc::Status::OK;
        }
        catch(std::exception& e)
        {
            LOG_EXCEPTION(e.what());
            return {grpc::StatusCode::INTERNAL, e.what()};
        }
        catch(...)
        {
            LOG_EXCEPTION("Unknown error.");
            return {grpc::StatusCode::UNKNOWN, "Unknown exception occurred."};
        }
    }

}








