//
// Created by ivan on 16.06.21.
//

#include "Server.h"

#include <utility>

namespace server
{

    Server::Server(std::string hostAddress,
                   uint16_t port,
                   std::filesystem::path rootDirPath) :
           m_hostAddress(std::move(hostAddress)), m_port(port), m_rootDirectoryPath(std::move(rootDirPath))
    {}

    Server::Server(const std::filesystem::path &confFile)
    {
        // TODO: read parameters from confFile
    }
    void Server::Run()
    {
        std::string server_address(this->m_hostAddress + ":" + std::to_string(this->m_port));
        FTServiceImpl service(this->m_rootDirectoryPath);
        grpc::ServerBuilder builder;
        builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
        builder.RegisterService(&service);
        std::unique_ptr<grpc::Server> server(builder.BuildAndStart());
        std::cout << "server listening on " << server_address << '\n';
        server->Wait();
    }

}