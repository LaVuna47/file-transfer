//
// Created by ivan on 31.07.21.
//

#ifndef FILETRANSFER_COLORS_H
#define FILETRANSFER_COLORS_H

#include <string>

namespace color
{
    using std::string;

    string GetColoredString(const string& str)
    {
        return "\033[30m" + str + "\033[0m";
    }
}
#endif //FILETRANSFER_COLORS_H

/*
int main(int argc, char ** argv)
{
    po::options_description global("Global options");
    global.add_options()
    ("debug", "Turn on debug output")
    ("command", po::value<std::string>(), "command to execute")
    ("subargs", po::value<std::vector<std::string> >(), "Arguments for command");

    po::positional_options_description pos;
    pos.add("command", 1).
    add("subargs", -1);

    po::variables_map vm;

    po::parsed_options parsed = po::command_line_parser(argc, argv).options(global).positional(pos).allow_unregistered().run();

    po::store(parsed, vm);

    std::string cmd = vm["command"].as<std::string>();
    if (cmd == "ls")
    {
        // ls command has the following options:
        po::options_description ls_desc("ls options");
        ls_desc.add_options()
        ("hidden", "Show hidden files")
        ("path", po::value<std::string>(), "Path to list");

        // Collect all the unrecognized options from the first pass. This will include the
        // (positional) command name, so we need to erase that.
        std::vector<std::string> opts = po::collect_unrecognized(parsed.options, po::include_positional);
        opts.erase(opts.begin());

        // Parse again...
        po::store(po::command_line_parser(opts).options(ls_desc).run(), vm);
    }


}
 */