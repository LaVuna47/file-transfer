//
// Created by ivan on 31.07.21.
//

#ifndef FILETRANSFER_CONSOLE_H
#define FILETRANSFER_CONSOLE_H
#include "Settings.h"

class Console
{
public:
    Console(const Settings& settings = {});

    void Run();
};


#endif //FILETRANSFER_CONSOLE_H
