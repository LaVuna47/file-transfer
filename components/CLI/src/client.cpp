//
// Created by ivan on 15.06.21.
//
#include "ClientManager.h"
#include "Colors.h"

// boost
#include <boost/program_options.hpp>
namespace
{
    const size_t ERROR_IN_COMMAND_LINE = 1;
    const size_t SUCCESS = 0;
    const size_t ERROR_UNHANDLED_EXCEPTION = 2;

} // namespace

int main(int argc, char** argv)
{
    try
    {
        namespace po = boost::program_options;
        using std::cout, std::vector, std::string;

        // Creating clientManager
        /// TODO: rewrite it to read from config file and add options to config this from CLI
        std::unique_ptr<client::IClientManager> clientManager = std::make_unique<client::ClientManagerCoreImpl>("192.168.14.16",5555);


        /** Define and parse the program options  */
        po::options_description global("Global options");
        global.add_options()
        ("help", "print help message")
        ("command", "command to execute")
        ("subargs", po::value<std::vector<std::string> >(), "Arguments for command");

        po::positional_options_description positionalOptions;
        positionalOptions.add("command", 1).add("subargs", -1);

        po::variables_map vm;
        try
        {
            po::parsed_options parsed = po::command_line_parser(argc, argv).options(global).positional(positionalOptions).allow_unregistered().run();
            po::store(parsed, vm);

            // global options
            if(vm.count("help"))
            {
                std::cout << global << '\n';
                return SUCCESS;
            }

            std::string cmd;
            if(vm.count("command"))
                cmd = vm["command"].as<std::string>();

            if ( cmd == "push" )
            {
                // push command has the following options:
                po::options_description push_desc("push options");
                push_desc.add_options()
                ("help", "prints help message for push command")
                ("files,f", po::value<std::vector<std::string>>() ,"files to be pushed")
                ("directory,d", po::value<std::vector<std::string>>(), "directory to be pushed")
                ("location,l", po::value<std::string>(), "location on server where specified files will be stored");

                std::vector<std::string> opts = po::collect_unrecognized(parsed.options, po::include_positional);
                opts.erase(opts.begin());

                // Parse
                po::store(po::command_line_parser(opts).options(push_desc).run(), vm);
                if(vm.count("help"))
                {
                    std::cout << push_desc << '\n';
                }
                else if(vm.count("files") || vm.count("directory"))
                {
                    std::string location = "./";
                    std::vector<std::string> inputFiles;
                    std::vector<std::string> inputDirectories;

                    if(vm.count("files"))
                        inputFiles = vm["files"].as<std::vector<std::string>>();

                    if(vm.count("directory"))
                        inputDirectories = vm["directory"].as<std::vector<std::string>>();

                    if(vm.count("location"))
                        location = vm["location"].as<std::string>();

                    vector<std::tuple<std::filesystem::path,std::filesystem::path>> filePaths, dirPaths;
                    filePaths.reserve(inputFiles.size());
                    dirPaths.reserve(inputFiles.size());

                    for(const auto& file: inputFiles)
                    {
                        decltype(auto) filePath = std::filesystem::path(file);
                        if(!std::filesystem::exists(filePath))
                        {
                            std::cout << "File " << file << " does not exist. It won't we transferred.\n";
                            continue;
                        }
                        filePaths.emplace_back(filePath, location + "/" + filePath.filename().string());
                    }
                    for(const auto& dir: inputDirectories)
                    {
                        decltype(auto) dirPath = std::filesystem::path(dir);
                        if(!std::filesystem::exists(dirPath))
                        {
                            std::cout << "Directory " << dir << " does not exist. It won't we transferred.";
                            continue;
                        }
                        dirPaths.emplace_back(dirPath, location);
                    }
                    if(!filePaths.empty())
                    {
                        ft::Response response = clientManager->PushFiles(filePaths);
                        std::cout << response.message() << '\n';
                    }
                    if(!dirPaths.empty())
                    {
                        ft::Response response = clientManager->PushDirectories(dirPaths);
                        std::cout << response.message() << '\n';
                    }
                }
                else
                {
                    std::cout << push_desc << '\n';
                }
                return SUCCESS;
            }
            else if ( cmd == "pull" )
            {
                // pull command has the following options:
                po::options_description push_desc("pull options");
                push_desc.add_options()
                ("help", "prints help message for pull command")
                ("file,f", po::value<std::vector<std::string>>() ,"files to be pulled")
                ("directory,d", po::value<std::vector<std::string>>(), "directory to be pulled")
                ("location,l", po::value<std::string>(), "location where files, directory will be put");

                std::vector<std::string> opts = po::collect_unrecognized(parsed.options, po::include_positional);
                opts.erase(opts.begin());

                // Parse
                po::store(po::command_line_parser(opts).options(push_desc).run(), vm);

                if(vm.count("help"))
                {
                    std::cout << push_desc << '\n';
                }
                else if(vm.count("file") || vm.count("directory"))
                {
                    std::string location = "./";
                    std::vector<std::string> inputFiles;
                    std::vector<std::string> inputDirectories;

                    if(vm.count("file"))
                        inputFiles = vm["file"].as<std::vector<std::string>>();

                    if(vm.count("directory"))
                        inputDirectories = vm["directory"].as<std::vector<std::string>>();

                    if(vm.count("location"))
                        location = vm["location"].as<std::string>();

                    if(!std::filesystem::exists(location))
                    {
                        std::cout << "Location " << location << " does not exist.\n";
                        return ERROR_IN_COMMAND_LINE;
                    }

                    vector<std::tuple<std::filesystem::path,std::filesystem::path>> filePaths, dirPaths;
                    filePaths.reserve(inputFiles.size());
                    dirPaths.reserve(inputFiles.size());

                    for(const auto& file: inputFiles)
                    {
                        decltype(auto) filePath = std::filesystem::path(file);
                        filePaths.emplace_back(location + "/" + filePath.filename().string(), filePath);
                    }
                    for(const auto& dir: inputDirectories)
                    {
                        dirPaths.emplace_back(location, dir);
                    }

                    if(!filePaths.empty())
                    {
                        ft::Response response = clientManager->PullFiles(filePaths);
                        std::cout << response.message() << '\n';
                    }
                    if(!dirPaths.empty())
                    {
                        ft::Response response = clientManager->PullDirectories(dirPaths);
                        std::cout << response.message() << '\n';
                    }
                }
                else
                {
                    std::cout << push_desc << '\n';
                }
                return SUCCESS;
            }
            else if( cmd == "search")
            {
                // pull command has the following options:
                po::options_description push_desc("pull options");
                push_desc.add_options()
                ("help", "prints help message for search command")
                ("regex,r", po::value<std::string>() ,"regular expression");

                std::vector<std::string> opts = po::collect_unrecognized(parsed.options, po::include_positional);
                opts.erase(opts.begin());

                // Parse
                po::store(po::command_line_parser(opts).options(push_desc).run(), vm);

                if(vm.count("help"))
                {
                    std::cout << push_desc << '\n';
                }
                if(vm.count("regex"))
                {
                    decltype(auto) regex = vm["regex"].as<std::string>();
                    std::vector<std::string> outFiles;
                    ft::Response response = clientManager->SearchForFiles(&outFiles, regex);
                    if(response.status() == ft::Response::SUCCESS)
                    {
                        std::for_each(outFiles.cbegin(), outFiles.cend(), [](const std::string& str){std::cout << str << '\n';});
                    }
                    std::cout << response.message() << '\n';
                }
                else
                {
                    std::cout << push_desc << '\n';
                }
                return SUCCESS;
            }
            else if(cmd == "tree-view")
            {
                std::shared_ptr<impl::FilesystemTree> filesystemTree;
                ft::Response response = clientManager->GetFilesystemTreeView(filesystemTree,".");
                std::cout << response.message() << '\n';
                filesystemTree->print();
            }
            else if(cmd == "delete")
            {
                // pull command has the following options:
                po::options_description push_desc("pull options");
                push_desc.add_options()
                ("help", "prints help message for search command")
                ("file,f", po::value<std::vector<std::string>>() ,"file to be removed")
                ("directory,d", po::value<std::vector<std::string>>(), "directory to be removed")
                ("regex,r", po::value<std::string>(), "regular expression");

                std::vector<std::string> opts = po::collect_unrecognized(parsed.options, po::include_positional);
                opts.erase(opts.begin());

                // Parse
                po::store(po::command_line_parser(opts).options(push_desc).run(), vm);

                /// TODO: implement this
                if(vm.count("help"))
                {
                    std::cout << push_desc << '\n';
                }
                else if(vm.count("file") || vm.count("directory"))
                {
                    std::vector<std::string> inputDirectories, inputFiles;
                    if(vm.count("file"))
                        inputFiles = vm["file"].as<std::vector<std::string>>();
                    if(vm.count("directory"))
                        inputDirectories = vm["directory"].as<std::vector<std::string>>();

                    std::vector<std::filesystem::path> filePaths, dirPaths;
                    std::for_each(std::cbegin(inputFiles), std::cend(inputFiles), [&filePaths](const std::string& str) {
                        filePaths.emplace_back(str);});
                    std::for_each(std::cbegin(inputDirectories), std::cend(inputDirectories), [&dirPaths](const std::string& str) {
                        dirPaths.emplace_back(str);});

                    if(!filePaths.empty())
                    {
                        ft::Response response = clientManager->DeleteFiles(filePaths);
                        std::cout << response.message() << '\n';
                    }
                    if(!dirPaths.empty())
                    {
                        ft::Response response = clientManager->DeleteDirectories(dirPaths);
                        std::cout << response.message() << '\n';
                    }
                }
                else if(vm.count("regex"))
                {
                    decltype(auto) regex = vm["regex"].as<std::string>();

                    ft::Response response = clientManager->Delete(regex);
                    std::cout << response.message();
                }
                else
                {
                    std::cout << push_desc << '\n';
                }
                return SUCCESS;
            }
            else
            {
                std::cout << global << '\n';
            }
            po::notify(vm); // throws on error, so do after help in case
            // there are any problems
        }
        catch(po::error& e)
        {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << global << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }

        // application code here //

    }
    catch(std::exception& e)
    {
        std::cerr << "Unhandled Exception reached the top of main: "
        << e.what() << ", application will now exit" << std::endl;
        return ERROR_UNHANDLED_EXCEPTION;

    }

    return SUCCESS;

} // main
