//
// Created by rainier on 04.06.21.
//

#include "Helper.h"

#define LOG_DEBUG
#define LOG_AND_THROW
#define LOG_EXCEPTION
#define LOG_INFO
#define LOG_NOTICE
#define LOG_TRACE
#define LOG_WARN

namespace helper
{
    std::string Execute(const std::string &cmd)
    {
        std::array<char, 128> buffer{};
        std::string result;
        std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd.c_str(), "r"), pclose);
        if (!pipe)
        {
            throw std::runtime_error("popen() failed!");
        }
        while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr)
        {
            result += buffer.data();
        }
        return result;
    }

    void Archive(const std::filesystem::path &source,
                 const std::filesystem::path &dest)
    {
        std::filesystem::current_path( source.parent_path());
        boost::process::system("zip -r " + dest.string() + " " + source.filename().string());
    }

    void Extract(const std::filesystem::path &source, const std::filesystem::path &dest)
    {
        boost::process::system(
                "unzip " + source.string() + " -d " + dest.string());
    }
}