//
// Created by rainier on 04.06.21.
//

#ifndef FILETRANSFER_HELPER_H
#define FILETRANSFER_HELPER_H

#include <string>
#include <array>
#include <memory>
#include <filesystem>
#include <boost/process.hpp>

namespace helper
{
    /**
     * @param cmd : command to be executed.
     * @return result of executed command
     */
    std::string Execute(const std::string& cmd);
    /**
     * Archives file or directory using zip archiver.
     *
     * @param source: path(directory or file) that is going to be archived.
     * @param dest: path with filename ending(some/dest/file.zip) where archived file will be put.
     */
    void Archive(const std::filesystem::path& source, const std::filesystem::path& dest);
    /**
     * Extracts zip archive.
     *
     * @param source: path to archived file.
     * @param dest: directory where archived content will be put.
     */
    void Extract(const std::filesystem::path& source, const std::filesystem::path& dest);

}


#endif //FILETRANSFER_HELPER_H
