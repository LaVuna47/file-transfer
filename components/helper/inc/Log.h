//
// Created by ivan on 01.08.21.
//

#ifndef FILETRANSFER_LOG_H
#define FILETRANSFER_LOG_H
#include <sstream>
#include <iostream>

#define LOG_ERROR(msg) \
{                      \
    std::stringstream ss; \
    ss << "\033[31m"; \
    ss << "Error: " << msg; \
    ss << "\033[0m" << '\n'; \
    std::cout << ss.str() << std::flush; \
}


#define LOG_STATUS(msg) \
{                      \
    std::stringstream ss; \
    ss << "\033[0m"; \
    ss << "Status: " << msg; \
    ss << "\033[0m" << '\n'; \
    std::cout << ss.str() << std::flush; \
}

#define LOG_EXCEPTION(msg) \
{                      \
    std::stringstream ss; \
    ss << "\033[31,1m"; \
    ss << "EXCEPTION: " << msg; \
    ss << "\033[0m" << '\n'; \
    std::cout << ss.str() << std::flush; \
}

#define LOG_MESSAGE(msg) \
{                      \
    std::stringstream ss; \
    ss << "\033[32m"; \
    ss << "Message: " << msg; \
    ss << "\033[0m" << '\n'; \
    std::cout << ss.str() << std::flush; \
}
#endif //FILETRANSFER_LOG_H
