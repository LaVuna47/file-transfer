//
// Created by rainier on 30.07.21.
//

#ifndef FILETRANSFER_FILESYSTEMTREE_H
#define FILETRANSFER_FILESYSTEMTREE_H

#include <filesystem>
#include <iostream>
#include <map>
#include <utility>
#include <vector>
#include <cassert>
#include <functional>
#include <stack>

namespace impl
{
    class FilesystemTree : std::enable_shared_from_this<FilesystemTree>
    {
    private:
        struct Node
        {
            std::string data;
            std::map<std::string,Node*> children;
            Node()=default;
            explicit Node(std::string  data): data(std::move(data)){}
            bool operator>(const Node& r) const
            {
                return this->data > r.data;
            }
        };

    private:
        void Construct(const std::filesystem::path& path);
        // push some filesystem path into tree
        void Push(const std::filesystem::path& path);

    private:
        Node* m_root;

    public:

        // constructors / destructor
        explicit FilesystemTree(const std::filesystem::path& path);
        explicit FilesystemTree(const std::vector<std::filesystem::path>& paths);
        ~FilesystemTree()=default;

        // core
        void print();

    };


}

#endif //FILETRANSFER_FILESYSTEMTREE_H
