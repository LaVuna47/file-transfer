//
// Created by ivan on 02.06.21.
//

#include <ft.pb.h>
#include <utility>
#include "ClientManager.h"


namespace client
{
    const std::uint32_t BUF_SIZE = 65536;

    ClientBase::ClientBase(std::string serverHost, uint16_t serverPort) :
            c_serverHost(std::move(serverHost)), c_serverPort(serverPort)
    {
        // init client stub
        c_clientStub = ft::FileTransfer::NewStub(
                grpc::CreateChannel(c_serverHost + ":" + std::to_string(c_serverPort), grpc::InsecureChannelCredentials()));
    }

    ft::Response ClientBase::PushFile(const std::filesystem::path &clientPath,
                                      const std::filesystem::path &serverPath) const noexcept
    {
        ft::Response response;
        try
        {
            const auto start = std::chrono::high_resolution_clock::now();

            if (!std::filesystem::exists(clientPath.string()))
            {
                response.set_status(ft::Response::FAILURE);
                response.set_message("File " + clientPath.string() + " does not exist.");
                return response;
            }

            grpc::ClientContext clientContext;
            std::unique_ptr<grpc::ClientWriter<ft::FileChunk>> writer(c_clientStub->PushFile(&clientContext, &response));
            ft::FileChunk fileChunk;
            std::string serverPathStr = serverPath.string() + "/" + clientPath.filename().string();
            fileChunk.set_path(serverPathStr);

            char buffer[BUF_SIZE];
            memset(buffer, '\0', BUF_SIZE);

            std::fstream rStream(clientPath.string(), std::ios::in | std::ios::binary);
            if (!rStream.is_open())
            {
                response.set_status(ft::Response::FAILURE);
                response.set_message("Could not open " + clientPath.string());
                return response;
            }

            for (std::streamsize bytesRead; true;)
            {
                bytesRead = rStream.readsome(buffer, BUF_SIZE);
                fileChunk.set_datablock(buffer, bytesRead);
                writer->Write(fileChunk);
                fileChunk.clear_datablock();
                if (bytesRead < BUF_SIZE)
                    break;
            }

            rStream.close();
            writer->WritesDone();

            grpc::Status status = writer->Finish();
            if (!status.ok())
            {
                response.set_status(ft::Response::FAILURE);
                response.set_message("Server error: " + status.error_message());
                return response;
            }

            const auto end = std::chrono::high_resolution_clock::now();
            const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
            response.set_timeelapsed(static_cast<double>(duration.count()) * 0.001);

            return response;
        }
        catch (std::exception& e)
        {
            response.set_status(ft::Response::FAILURE);
            response.set_message("Exception: " + std::string(e.what()));
            return response;
        }
        catch(...)
        {
            response.set_status(ft::Response::FAILURE);
            response.set_message("Exception: Unknown error");
            return response;
        }
    }

    ft::Response ClientBase::PullFile(const std::filesystem::path &clientPath,
                                      const std::filesystem::path &serverPath) const noexcept
    {
        ft::Response response;
        try
        {
            const auto start = std::chrono::high_resolution_clock::now();
            grpc::ClientContext clientContext;
            ft::FilePath path;
            path.set_path(serverPath.string());
            std::unique_ptr<grpc::ClientReader<ft::FileChunk>> reader(c_clientStub->PullFile(&clientContext, path));
            ft::FileChunk chunk;
            std::filesystem::path clientFullPath = clientPath.string() + "/" + serverPath.filename().string();

            if(!std::filesystem::exists(clientFullPath.parent_path()))
                std::filesystem::create_directories(clientFullPath.parent_path());

            std::fstream wStream(clientFullPath.string(), std::ios::out | std::ios::binary);
            if (!wStream.is_open())
            {
                response.set_status(ft::Response::FAILURE);
                response.set_message("Could not open " + clientPath.string());
                return response;
            }

            for (; reader->Read(&chunk);)
            {
                wStream.write(chunk.datablock().c_str(), static_cast<std::streamsize>(chunk.datablock().size()));
            }

            wStream.close();
            grpc::Status status = reader->Finish();

            if (!status.ok())
            {
                response.set_status(ft::Response::FAILURE);
                response.set_message("Server error: " + status.error_message());
                return response;
            }

            // TODO: rewrite this implementation in nicer way
            const auto end = std::chrono::high_resolution_clock::now();
            const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

            response.set_timeelapsed(static_cast<double>(duration.count()) * 0.001);
            response.set_message("File " + clientPath.string() + " was delivered successfully");
            response.set_status(ft::Response_STATUS_SUCCESS);

            return response;
        }
        catch (std::exception& e)
        {
            response.set_status(ft::Response::FAILURE);
            response.set_message("Exception: " + std::string(e.what()));
            return response;
        }
        catch(...)
        {
            response.set_status(ft::Response::FAILURE);
            response.set_message("Exception: Unknown error");
            return response;
        }
    }

    ft::Response ClientBase::Extract(const std::filesystem::path &serverPath) const noexcept
    {
        ft::Response response;

        try
        {
            const auto start = std::chrono::high_resolution_clock::now();

            grpc::ClientContext clientContext;
            ft::ArchivePath path;
            path.set_path(serverPath.string());

            grpc::Status status = c_clientStub->Extract(&clientContext, path, &response);
            if (!status.ok())
            {
                response.set_status(ft::Response::FAILURE);
                response.set_message("Server error: " + status.error_message());
                return response;
            }

            // TODO: rewrite this implementation in nicer way
            const auto end = std::chrono::high_resolution_clock::now();
            const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
            response.set_timeelapsed(static_cast<double>(duration.count()) * 0.001);

            return response;
        }
        catch (std::exception& e)
        {
            response.set_status(ft::Response::FAILURE);
            response.set_message("Exception: " + std::string(e.what()));
            return response;
        }
        catch(...)
        {
            response.set_status(ft::Response::FAILURE);
            response.set_message("Exception: Unknown error");
            return response;
        }
    }

    ft::Response ClientBase::Search(std::vector<std::string>* resFiles, const std::string &regex) const noexcept
    {
        ft::Response response;
        try
        {
            const auto start = std::chrono::high_resolution_clock::now();
            grpc::ClientContext clientContext;
            ft::FilePath path;
            ft::Regex regexRequest;
            regexRequest.set_regex(regex);

            std::unique_ptr<grpc::ClientReader<ft::FilePath>> reader(
                    c_clientStub->Search(&clientContext, regexRequest));

            for (; reader->Read(&path);)
            {
                resFiles->emplace_back(path.path());
            }

            grpc::Status status = reader->Finish();

            if (!status.ok())
            {
                response.set_status(ft::Response::FAILURE);
                response.set_message("Server error: " + status.error_message());
                return response;
            }

            // TODO: rewrite this implementation in nicer way
            const auto end = std::chrono::high_resolution_clock::now();
            const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

            response.set_timeelapsed(static_cast<double>(duration.count()) * 0.001);
            response.set_status(ft::Response::SUCCESS);
            response.set_message("Found " + std::to_string(resFiles->size()) + " files.");
            return response;
        }
        catch (std::exception& e)
        {
            response.set_status(ft::Response::FAILURE);
            response.set_message("Exception: " + std::string(e.what()));
            return response;
        }
        catch(...)
        {
            response.set_status(ft::Response::FAILURE);
            response.set_message("Exception: Unknown error");
            return response;
        }
    }

    ft::Response ClientBase::DeleteFile(const std::filesystem::path &serverPath) const noexcept
    {
        ft::Response response;

        try
        {
            const auto start = std::chrono::high_resolution_clock::now();

            grpc::ClientContext clientContext;
            ft::FilePath path;
            path.set_path(serverPath.string());

            grpc::Status status = c_clientStub->DeleteFile(&clientContext, path, &response);
            if (!status.ok())
            {
                response.set_status(ft::Response::FAILURE);
                response.set_message("Server error: " + status.error_message());
                return response;
            }

            // TODO: rewrite this implementation in nicer way
            const auto end = std::chrono::high_resolution_clock::now();
            const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
            response.set_timeelapsed(static_cast<double>(duration.count()) * 0.001);

            return response;
        }
        catch (std::exception& e)
        {
            response.set_status(ft::Response::FAILURE);
            response.set_message("Exception: " + std::string(e.what()));
            return response;
        }
        catch(...)
        {
            response.set_status(ft::Response::FAILURE);
            response.set_message("Exception: Unknown error");
            return response;
        }
    }

    ft::Response ClientBase::DeleteDirectory(const std::filesystem::path &serverPath) const noexcept
    {
        ft::Response response;
        try
        {
            const auto start = std::chrono::high_resolution_clock::now();

            grpc::ClientContext clientContext;
            ft::DirectoryPath dirPath;
            dirPath.set_path(serverPath.string());

            grpc::Status status = c_clientStub->DeleteDirectory(&clientContext, dirPath, &response);
            if (!status.ok())
            {
                response.set_status(ft::Response::FAILURE);
                response.set_message("Server error: " + status.error_message());
                return response;
            }

            // TODO: rewrite this implementation in nicer way
            const auto end = std::chrono::high_resolution_clock::now();
            const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
            response.set_timeelapsed(static_cast<double>(duration.count()) * 0.001);

            return response;
        }
        catch (std::exception& e)
        {
            response.set_status(ft::Response::FAILURE);
            response.set_message("Exception: " + std::string(e.what()));
            return response;
        }
        catch(...)
        {
            response.set_status(ft::Response::FAILURE);
            response.set_message("Exception: Unknown error");
            return response;
        }
    }

    ft::Response ClientBase::Delete(const std::string &regex) const noexcept
    {
        ft::Response response;

        try
        {
            const auto start = std::chrono::high_resolution_clock::now();

            grpc::ClientContext clientContext;
            ft::Regex regexRequest;
            regexRequest.set_regex(regex);

            grpc::Status status = c_clientStub->Delete(&clientContext, regexRequest, &response);
            if (!status.ok())
            {
                response.set_status(ft::Response::FAILURE);
                response.set_message("Server error: " + status.error_message());
                return response;
            }

            // TODO: rewrite this implementation in nicer way
            const auto end = std::chrono::high_resolution_clock::now();
            const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
            response.set_timeelapsed(static_cast<double>(duration.count()) * 0.001);

            return response;
        }
        catch (std::exception& e)
        {
            response.set_status(ft::Response::FAILURE);
            response.set_message("Exception: " + std::string(e.what()));
            return response;
        }
        catch(...)
        {
            response.set_status(ft::Response::FAILURE);
            response.set_message("Exception: Unknown error");
            return response;
        }

    }

    ft::Response ClientBase::Archive(const std::filesystem::path &serverPath) const noexcept
    {
        ft::Response response;
        try
        {
            const auto start = std::chrono::high_resolution_clock::now();

            grpc::ClientContext clientContext;
            ft::DirectoryPath path;
            path.set_path(serverPath.string());

            grpc::Status status = c_clientStub->Archive(&clientContext, path, &response);
            if (!status.ok())
            {
                response.set_status(ft::Response::FAILURE);
                response.set_message("Server error: " + status.error_message());
                return response;
            }

            // TODO: rewrite this implementation in nicer way
            const auto end = std::chrono::high_resolution_clock::now();
            const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
            response.set_timeelapsed(static_cast<double>(duration.count()) * 0.001);

            return response;
        }
        catch (std::exception& e)
        {
            response.set_status(ft::Response::FAILURE);
            response.set_message("Exception: " + std::string(e.what()));
            return response;
        }
        catch(...)
        {
            response.set_status(ft::Response::FAILURE);
            response.set_message("Exception: Unknown error");
            return response;
        }
    }

    IClientManager::IClientManager(std::string serverHost, uint16_t serverPort) :
        ClientBase(std::move(serverHost), serverPort)
    {}

/* ************************************************************************ *
 * ClientManagerCoreIml
 * ************************************************************************ *
 */
    ClientManagerCoreImpl::ClientManagerCoreImpl(std::string serverHost, uint16_t serverPort) :
        IClientManager(std::move(serverHost), serverPort)
    {}

    ft::Response ClientManagerCoreImpl::PushFile(const std::filesystem::path &clientPath,
                                                 const std::filesystem::path &serverPath) const
    {
        return ClientBase::PushFile(clientPath, serverPath);
    }

    ft::Response ClientManagerCoreImpl::PullFile(const std::filesystem::path &clientPath,
                                                 const std::filesystem::path &serverPath) const
    {
        return ClientBase::PullFile(clientPath, serverPath);
    }

    ft::Response ClientManagerCoreImpl::PushFiles(
            const std::vector<std::tuple<std::filesystem::path, std::filesystem::path>> &client_serverPaths) const
    {
        ft::Response response, curResponse;
        double timeElapsed = 0.;
        for(const auto& item: client_serverPaths)
        {
            curResponse = ClientBase::PushFile(std::get<0>(item),std::get<1>(item));
            timeElapsed += curResponse.timeelapsed();
            if(curResponse.status() != ft::Response_STATUS_SUCCESS)
            {
                return curResponse;
            }
        }
        response.set_status(ft::Response_STATUS_SUCCESS);
        response.set_message("Files where pushed successfully.");
        response.set_timeelapsed(timeElapsed);

        return response;
    }

    ft::Response ClientManagerCoreImpl::PullFiles(
            const std::vector<std::tuple<std::filesystem::path, std::filesystem::path>> &client_serverPaths) const
    {
        ft::Response response, curResponse;
        double timeElapsed = 0.;
        for(const auto& item: client_serverPaths)
        {
            curResponse = ClientBase::PullFile(std::get<0>(item),std::get<1>(item));
            timeElapsed += curResponse.timeelapsed();
            if(curResponse.status() != ft::Response_STATUS_SUCCESS)
            {
                return curResponse;
            }
        }
        response.set_status(ft::Response_STATUS_SUCCESS);
        response.set_message("Files where pulled successfully.");
        response.set_timeelapsed(timeElapsed);

        return response;
    }

    ft::Response ClientManagerCoreImpl::PushDirectory(const std::filesystem::path &clientPath,
                                                      const std::filesystem::path &serverPath) const
    {
        ft::Response response;
        try
        {
            auto start = std::chrono::high_resolution_clock::now();

            const std::string zipped_filename = clientPath.filename().string() + ".ft_zip";
            std::string cliPath = clientPath.parent_path().string() + "/" + zipped_filename;
            // archive directory
            helper::Archive(clientPath, clientPath.parent_path().string() + "/" + zipped_filename);
            // push file
            response = ClientBase::PushFile(clientPath.parent_path().string() + "/" + zipped_filename,
                                 serverPath.string() + "/" + zipped_filename);
            if(response.status() != ft::Response_STATUS_SUCCESS)
                return response;

            // extract  file
            response = ClientBase::Extract(serverPath.parent_path().string() + "/" + zipped_filename);
            if(response.status() != ft::Response_STATUS_SUCCESS)
                return response;

            // cleaning server archive
            response = ClientBase::DeleteFile(serverPath.parent_path().string() + "/" + zipped_filename);
            if(response.status() != ft::Response_STATUS_SUCCESS)
                return response;
            // cleaning client archive
            std::filesystem::remove(clientPath.parent_path().string() + "/" + zipped_filename);

            response.set_status(ft::Response_STATUS_SUCCESS);
            response.set_message("Directory " + clientPath.string() + " was pushed successfully.");

            auto end = std::chrono::high_resolution_clock::now();

            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
            response.set_timeelapsed(static_cast<double>(duration.count()) * 0.001);
            return response;
        }
        catch (std::exception& e)
        {
            response.set_status(ft::Response_STATUS_FAILURE);
            response.set_message("Exception: " + std::string(e.what()));
            return response;
        }
        catch (...)
        {
            response.set_status(ft::Response_STATUS_FAILURE);
            response.set_message("Exception: Unknown error.");
            return response;
        }
    }

    ft::Response ClientManagerCoreImpl::PullDirectory(const std::filesystem::path &clientPath,
                                                      const std::filesystem::path &serverPath) const
    {
        ft::Response response;
        try
        {
            auto start = std::chrono::high_resolution_clock::now();

            const std::string& archiveName = serverPath.filename().string() + ".ft_zip";

            response = ClientBase::Archive(serverPath);
            if(response.status() != ft::Response::SUCCESS)
                return response;

            response = ClientBase::PullFile(clientPath.string() + "/" + archiveName ,serverPath.parent_path().string() + "/" + archiveName);
            if(response.status() != ft::Response::SUCCESS)
                return response;

            helper::Extract(clientPath.string() + "/" + archiveName,clientPath.string());

            // cleaning server
            response = ClientBase::DeleteFile(archiveName);
            if(response.status() != ft::Response::SUCCESS)
                return response;

            // cleaning client
            std::filesystem::remove(clientPath.string() + "/" + archiveName);

            response.set_status(ft::Response_STATUS_SUCCESS);
            response.set_message("Directory " + serverPath.string() + " was pulled successfully.");

            auto end = std::chrono::high_resolution_clock::now();

            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
            response.set_timeelapsed(static_cast<double>(duration.count()) * 0.001);
            return response;
        }
        catch (std::exception& e)
        {
            response.set_status(ft::Response_STATUS_FAILURE);
            response.set_message("Exception: " + std::string(e.what()));
            return response;
        }
        catch (...)
        {
            response.set_status(ft::Response_STATUS_FAILURE);
            response.set_message("Exception: Unknown error.");
            return response;
        }
    }

    ft::Response ClientManagerCoreImpl::PushDirectories(
            const std::vector<std::tuple<std::filesystem::path, std::filesystem::path>> &client_serverPaths) const
    {
        ft::Response response, curResponse;
        double timeElapsed = 0.;
        for(const auto& item: client_serverPaths)
        {
            curResponse = ClientManagerCoreImpl::PushDirectory(std::get<0>(item),std::get<1>(item));
            timeElapsed += curResponse.timeelapsed();
            if(curResponse.status() != ft::Response_STATUS_SUCCESS)
            {
                return curResponse;
            }
        }
        response.set_status(ft::Response_STATUS_SUCCESS);
        response.set_message("Directories where pushed successfully.");
        response.set_timeelapsed(timeElapsed);

        return response;
    }

    ft::Response ClientManagerCoreImpl::PullDirectories(
            const std::vector<std::tuple<std::filesystem::path, std::filesystem::path>> &client_serverPaths) const
    {
        ft::Response response, curResponse;
        double timeElapsed = 0.;
        for(const auto& item: client_serverPaths)
        {
            curResponse = ClientManagerCoreImpl::PullDirectory(std::get<0>(item),std::get<1>(item));
            timeElapsed += curResponse.timeelapsed();
            if(curResponse.status() != ft::Response_STATUS_SUCCESS)
            {
                return curResponse;
            }
        }
        response.set_status(ft::Response_STATUS_SUCCESS);
        response.set_message("Directories where pulled successfully.");
        response.set_timeelapsed(timeElapsed);

        return response;
    }

    ft::Response
    ClientManagerCoreImpl::SearchForFiles(std::vector<std::string> *outFiles, const std::string &regex) const
    {
        return ClientBase::Search(outFiles, regex);
    }

    ft::Response ClientManagerCoreImpl::DeleteFile(const std::filesystem::path &serverPath) const
    {
        return ClientBase::DeleteFile(serverPath);
    }

    ft::Response ClientManagerCoreImpl::DeleteFiles(const std::vector<std::filesystem::path> &serverPaths) const
    {
        ft::Response response, curResponse;
        double timeElapsed = 0.;
        for(const auto& curPath: serverPaths)
        {
            curResponse = ClientBase::DeleteFile(curPath);
            timeElapsed += curResponse.timeelapsed();
            if(curResponse.status() != ft::Response_STATUS_SUCCESS)
            {
                return curResponse;
            }
        }
        response.set_status(ft::Response_STATUS_SUCCESS);
        response.set_message("Files where deleted successfully.");
        response.set_timeelapsed(timeElapsed);
        return response;
    }

    ft::Response ClientManagerCoreImpl::DeleteDirectory(const std::filesystem::path &serverPath) const
    {
        return ClientBase::DeleteDirectory(serverPath);
    }

    ft::Response ClientManagerCoreImpl::DeleteDirectories(const std::vector<std::filesystem::path> &serverPaths) const
    {
        ft::Response response, curResponse;
        double timeElapsed = 0.;
        for(const auto& curPath: serverPaths)
        {
            curResponse = ClientBase::DeleteDirectory(curPath);
            timeElapsed += curResponse.timeelapsed();
            if(curResponse.status() != ft::Response_STATUS_SUCCESS)
            {
                return curResponse;
            }
        }
        response.set_status(ft::Response_STATUS_SUCCESS);
        response.set_message("Directories where deleted successfully.");
        response.set_timeelapsed(timeElapsed);
        return response;
    }

    ft::Response ClientManagerCoreImpl::Delete(const std::string &regex) const
    {
        return ClientBase::Delete(regex);
    }

    ft::Response ClientManagerCoreImpl::GetFilesystemTreeView(std::shared_ptr<impl::FilesystemTree> &tree,
                                                              const std::filesystem::path &serverPath) const
    {
        auto start = std::chrono::high_resolution_clock::now();
        std::vector<std::string> serverFiles;
        ft::Response response = ClientBase::Search(&serverFiles,".*");
        if(response.status() != ft::Response::SUCCESS)
            return response;

        std::vector<std::filesystem::path> serverPathFiles(serverFiles.begin(), serverFiles.end());
        tree = std::make_shared<impl::FilesystemTree>(serverPathFiles);

        auto end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(start - end);
        response.set_timeelapsed(static_cast<double>(duration.count()) * 0.001);
        response.set_message("Filesystem tree view was successfully created.");
        return response;
    }


}


