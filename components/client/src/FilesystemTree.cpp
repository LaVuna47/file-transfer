//
// Created by rainier on 30.07.21.
//

#include "FilesystemTree.h"

namespace impl
{

    FilesystemTree::FilesystemTree(const std::filesystem::path& path)
    {
        // init root
        m_root = new Node("/");

        // construct tree
        this->Construct(path);
    }

    FilesystemTree::FilesystemTree(const std::vector<std::filesystem::path> &paths)
    {
        // init root
        m_root = new Node("/");

        for(const auto& path: paths)
        {
            FilesystemTree::Push(path);
        }
    }


    void FilesystemTree::Construct(const std::filesystem::path& path)
    {
        std::filesystem::recursive_directory_iterator rIter(path);
        for(const auto& item: rIter)
        {
            FilesystemTree::Push(item);
        }
    }
    void FilesystemTree::Push(const std::filesystem::path &path)
    {
        Node* node_iter = m_root;
        bool new_branch = false;
        for(auto iter = ++path.begin(); iter != path.end(); ++iter)
        {
            if(new_branch)
            {
                Node* new_node = new Node(iter->string());
                node_iter->children[new_node->data] = new_node;
                node_iter = new_node;
                continue;
            }

            if(node_iter->children.find(iter->string()) == node_iter->children.end())
            {
                new_branch = true;
                Node* new_node = new Node(iter->string());
                node_iter->children[new_node->data] = new_node;
                node_iter = new_node;
            }
            else
            {
                node_iter = node_iter->children[iter->string()];
            }
        }
    }

    void FilesystemTree::print()
    {
        Node* root = m_root;
        std::function<void(int, Node*)> print = [&](int w, Node* node) -> void
        {
            std::cout << std::string(w,' ') << node->data << '\n';
            for(const auto& ch: node->children)
            {
                print(w + 2, ch.second);
            }
        };
        print(0,root);
    }


}



int main(int argc, char** argv)
{
    impl::FilesystemTree tree("/home/ivan/CLionProjects/file-transfer");

    tree.print();

}
