# Function: find_files
# ========================
# Arguments: <dirPath> - desired path of filesystem inside of which we want to
#            search for files that match given <pattern>
#            <pattern> - specifies what we want to search(i.e: .txt or lib*.so)
# Performs:  Recursively iterates through all files inside <dirPath> and searches for files
#            with specified <pattern>
# Returns:   <Files> - all files that were found
# ========================
function(find_files dirPath pattern)
    set(Files "")
    file(GLOB_RECURSE GR "${dirPath}/" "${dirPath}/${pattern}")
    foreach(elem ${GR})
        list(APPEND Files ${elem})
    endforeach()
    set(Files ${Files} PARENT_SCOPE)
endfunction()

# Function: print_list
# ========================
# Arguments: <list> - list to printed
# Performs:  Prints all elements in list by one elem on one row.
# Returns:
# ========================
function(print_list list)
    foreach(elem ${list})
        message(">>>\t ${elem}")
    endforeach()
endfunction()
