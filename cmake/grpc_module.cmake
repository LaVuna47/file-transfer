# Function: generate_protobuf
# ========================
# Arguments: inputDir    - directory where lies .proto files.
#            outputDir   - directory where *.pb.cc & *.grpc.pb.h will be generated.
# Performs: Iterates through every .proto file in <inputDir> and generates
#           *.pb.h and *.pb.cc files into <outputDir>.
# Returns: GENERATED_SOURCE_FILES - list of all generated source files.
# ========================
function(generate_protobuf inputDir outputDir)
    set(GENERATED_SOURCE_FILES "")
    file(GLOB_RECURSE PROTO_FILES ${inputDir} ${inputDir}/*.proto)

    foreach(PROTO_FILE ${PROTO_FILES})
        string(REGEX REPLACE ${inputDir} ${outputDir} OUTPUT_SOURCE ${PROTO_FILE})
        string(REGEX REPLACE "[.]proto$" ".pb.cc" OUTPUT_SOURCE ${OUTPUT_SOURCE})
        list(APPEND GENERATED_SOURCE_FILES ${OUTPUT_SOURCE})
    endforeach()
    add_custom_command(OUTPUT
            ${GENERATED_SOURCE_FILES}
            COMMAND
            protoc  -I=${inputDir}
                    --cpp_out=${outputDir}
                    --proto_path=${inputDir}
            ${PROTO_FILES}
            DEPENDS ${PROTO_FILES})
    set(GENERATED_SOURCE_FILES ${GENERATED_SOURCE_FILES} PARENT_SCOPE)
endfunction()

# Function: generate_grpc
# ========================
# Arguments: inputDir    - directory where lies .proto files.
#            outputDir   - directory where *.grpc.pb.cc & *.grpc.pb.h
#                          will be generated.
# Performs: Iterates through every .proto file in <inputDir> and generates
#           *.grpc.pb.h and *.grpc.pb.cc files into <outputDir>.
# Returns: GENERATED_SOURCE_FILES - list of all generated source files.
# ========================
function(generate_grpc inputDir outputDir)
    set(GENERATED_SOURCE_FILES "")
    file(GLOB_RECURSE PROTO_FILES ${inputDir} ${inputDir}/*.proto)

    foreach(PROTO_FILE ${PROTO_FILES})
        string(REGEX REPLACE ${inputDir} ${outputDir} OUTPUT_SOURCE ${PROTO_FILE})
        string(REGEX REPLACE "[.]proto$" ".grpc.pb.cc" OUTPUT_SOURCE ${OUTPUT_SOURCE})
        list(APPEND GENERATED_SOURCE_FILES ${OUTPUT_SOURCE})
    endforeach()
    add_custom_command(OUTPUT
            ${GENERATED_SOURCE_FILES}
            COMMAND
            protoc  -I=${inputDir}
            --grpc_out=${outputDir}
            --proto_path=${inputDir}
            --plugin=protoc-gen-grpc=`which grpc_cpp_plugin`
            ${PROTO_FILES}
            DEPENDS ${PROTO_FILES})
    set(GENERATED_SOURCE_FILES ${GENERATED_SOURCE_FILES} PARENT_SCOPE)
endfunction()
